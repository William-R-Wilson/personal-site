class CreateWeeks < ActiveRecord::Migration[6.1]
  def change
    create_table :weeks do |t|
      t.string :state
      t.integer :year
      t.integer :week
      t.date :week_ending_date
      t.integer :all_cause
      t.integer :natural_cause
      t.integer :septicemia
      t.integer :malignant_neoplasms
      t.integer :diabetes_mellitus
      t.integer :alzheimers_disease
      t.integer :influenza_and_pneumonia
      t.integer :chronic_lower_respiratory
      t.integer :other_respiratory
      t.integer :nephritis
      t.integer :symptoms_signs_and_abnormal
      t.integer :diseases_of_heart
      t.integer :cerebrovascular_diseases
      t.integer :covid_19_multiple_cause_of_death
      t.integer :covid_19_underlying_cause_of_death
      t.timestamps
    end
  end
end

class AddPrivateToPosts < ActiveRecord::Migration[6.1]
  def change
    change_table :posts do |t|
      t.boolean :is_public, null: false, default: false
    end
  end
end

require "test_helper"

class PostsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @user = FactoryBot.create(:user)
    @post = FactoryBot.create(:post, is_public: true)
  end

  test "should get index" do
    get posts_url
    assert_response :success
  end

  test "cannot get new if not logged in" do
    get new_post_url
    assert_response :redirect
  end

  test "signed in can get edit" do
    sign_in(@user)
    get edit_post_url(@post)
    assert_response :success
  end


  test "signed in can get new" do
    sign_in(@user)
    get new_post_url
    assert_response :success
  end

  test "signed in should create post" do
    sign_in(@user)
    assert_difference('Post.count') do
      post posts_url, params: { post: { body: @post.body, title: @post.title } }
    end

    assert_redirected_to post_url(Post.last)
  end


  test "not signed in cannot create post" do
    assert_difference('Post.count', 0) do
      post posts_url, params: { post: { body: @post.body, title: @post.title } }
    end

    assert_redirected_to new_user_session_path
  end

  test "should show post" do
    get post_url(@post)
    assert_response :success
  end

  test "user not signed in cannot show post" do
    @priv_post = FactoryBot.create(:post, title: "private title", body: "private body", is_public: false)
    get post_url(@priv_post)
    assert_response :redirect
  end

  test "user signed in can show post" do
    @priv_post = FactoryBot.create(:post, title: "private title", body: "private body", is_public: false)
    get post_url(@priv_post)
    assert_response :redirect
  end

  test "cannot edit if not logged in" do
    get edit_post_url(@post)
    assert_response :redirect
  end

  test "signed in should update post" do
    sign_in(@user)
    patch post_url(@post), params: { post: { body: @post.body, title: @post.title } }
    assert_redirected_to post_url(@post)
  end

  test "not signed in cannot destroy post" do
    assert_difference('Post.count', 0) do
      delete post_url(@post)
    end
    assert_redirected_to new_user_session_path
  end

  test "signed in should destroy post" do
    sign_in(@user)
    assert_difference('Post.count', -1) do
      delete post_url(@post)
    end

    assert_redirected_to posts_url
  end

  test "index has post title and body" do
    get posts_url
    assert_select("h3", text: @post.title)
    assert_select("div.post-body", text: @post.body.to_plain_text)
  end

  test "index does not show private posts for user not signed in" do
    @post.update(is_public: false)
    get posts_url
    assert_select("div.post-body", false)
  end

  test "index shows private posts for signed in user" do
    @post.update(is_public: false)
    sign_in @user
    get posts_url
    assert_select("div.post-body", true)
  end

  test "index truncates long text at 400 characters" do
    @post.update(is_public: true, body: Faker::Lorem.paragraphs(number: 20).join.strip )
    get posts_url
    assert_select("div.post-body", text: @post.body.to_plain_text.strip[0...394] + "...")
  end

end

FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    password { "1234fght" }
    password_confirmation { "1234fght" }
  end
end

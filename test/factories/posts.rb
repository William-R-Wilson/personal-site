FactoryBot.define do
  factory :post do
    body { "Test body" }
    title  { "test title" }
  end
end
